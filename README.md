# PCU Platform EWAOL Build Tool

This project is forked from https://git.gitlab.arm.com/ewaol/meta-ewaol.git to support EWAOL image build with PCU as target hardware.

PCU Dev Board v2.0, designed by AutoCore, is a reference design hardware for automotive grade applications. For more details, please refer to [PCU specification](https://github.com/autocore-ai/autocore_pcu_doc/blob/master/docs/Pcu_specification.md).

Edge Workload Abstraction and Orchestration Layer (EWAOL) is the reference implementation for SOAFEE (Scalable Open Architecture For Embedded Edge), the Arm lead industry initiative for extending cloud-native software development to automotive, with a special focus on real-time and functional safety. For more details on SOAFEE, please see <http://soafee.io>.

For original EWAOL documentation could be found at <https://ewaol.sites.arm.com/meta-ewaol>.


## Build Host Setup

On the Build Host, install the kas setup tool:

```
sudo apt install python3-pip
sudo -H pip3 install kas
```

For more details on kas installation, see [kas Dependencies & installation](https://kas.readthedocs.io/en/latest/userguide.html#dependencies-installation).

For more details on kas, see [kas Introduction](https://kas.readthedocs.io/en/latest/intro.html).

> The Build Host machine should have at least 65 GBytes of free disk space for the next steps to work correctly.

## PCU Image Build

To build PCU image on host machine, run kas with the build configurations with the configuration file:

```
kas build meta-ewaol-config/kas/pcu.yml
```
Currently the following error will occur during compile, one config file need to be fixed manually:

change file `layers/meta-virtualization/recipes-containers/runc/runc-opencontainers_git.bb` with the following:

```diff
- git://github.com/opencontainers/runc;branch=master 
+ git://github.com/opencontainers/runc;branch=main
```

Then you could compile again.

## Image flash

The following images are supported by PCU, click on the link and follow the guideline to flash images:  
[x] [QSPI NOR flash](https://docs.nxp.com/bundle/GUID-E5527A77-2F97-4244-BF9C-D08F068EFD16/page/GUID-8DE07828-6197-4BED-AC95-D78B6DB98A50.html)  
[x] [SD card](https://docs.nxp.com/bundle/GUID-E5527A77-2F97-4244-BF9C-D08F068EFD16/page/GUID-4412C6E2-122B-4A1B-B2C6-0F082C1C5812.html)    
[x] [eMMC](https://docs.nxp.com/bundle/GUID-E5527A77-2F97-4244-BF9C-D08F068EFD16/page/GUID-612FCA1B-40EC-44C6-9A8D-69C5B3301A7F.html)

You could flash QSPI and eMMC/SD Card accoding to your setup.

## Run a demo

Now you can boot up the PCU board , login with user `root` and check the system information:

```bash
uname -a
Linux ls1046afrwy 5.10.72+ga68e31b63f86 #1 SMP REEMPT Tue Nov 23 06:02:20 UTC 2021 aarch64 arch64 aarch64 GNU/Linux
```

Then quickly check docker and k3s functionality by the following command:

```bash
docker -v
Docker version 20.10.3, build 41b3ea7e47
```

```bash
k3s --version
VERSION:
v1.20.11+k3s2 (9cb5fb57)
```

If it works, the system is ready to go.

## Repository License

The software is provided under an MIT license.

License details may be found in the [local license file](license.rst), or as
part of the project documentation.

Contributions to the project should follow the same license.
